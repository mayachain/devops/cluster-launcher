# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version     = "2.1.0"
  constraints = "~> 2.1"
  hashes = [
    "h1:/OpJKWupvFd8WJX1mTt8vi01pP7dkA6e//4l4C3TExE=",
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "h1:KfieWtVyGWwplSoLIB5usKAUnrIkDQBkWaR5TI+4WYg=",
    "h1:KtUCltnScfZbcvpE9wPH+a0e7KgMX4w7y8RSxu5J/NQ=",
    "h1:PaQTpxHMbZB9XV+c1od1eaUvndQle3ZZHx79hrI6C3k=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/linode/linode" {
  version     = "1.25.2"
  constraints = "~> 1.25"
  hashes = [
    "h1:AycQV64+4g8WASZh5tQcjELcXHlaLZq8Z5fJdsOCQZg=",
    "h1:Ora5l+Pm441IDYOxRHLWFrFZBrBxaa1s6X62Qp9KsFw=",
    "h1:lTcSZj98XhKRwngwEJDkuFsChaQG1UfWwrJnoTinXa8=",
    "h1:nz4lInCsnZfUeCSKoPsh3oRqyt/RoYOEhGqwvTbkDi8=",
    "h1:pP9DjC73rAbSh4KZB+Rzu6LcU++UoNL2+Vad6okihPM=",
    "zh:2dda53e1bdf5822d69eef381686af870b3db6c85a5f955392ef8616ffb6da10c",
    "zh:356b45396ebcbf965a5d3ac1e3e803832e5b7be36046eca74313963637e71372",
    "zh:377f424936b91a14f47d2b6ebbd7ecfdcff39ab58fe76b1800cb8d267c6c1e00",
    "zh:40ba9fe8b228adc5a648a85ad5fb37232f67324066d939573e67df109c415f21",
    "zh:5e39446c7f7c623f6d071a73ca352217b96da47a5605772c2bfed34f68038378",
    "zh:641a39a30ce6fbcf0e87ba9e32b65cc5ad7d45089d3ab9bc88f9480bb28f0107",
    "zh:9fa4983fa8693a1a6aefe6bcdf7fe08248b8e28c1cb1fb8e784fccb5a52dafbc",
    "zh:abf18db180b9a092256e83cba2313f595e9a5bfbc3c04d920e5593cafcfd466d",
    "zh:b94c6d1ef228abad88fb007b8b20b98d1fd85359cc03ef7ca02fa2e24afc4bf5",
    "zh:c1de155561187e3a3ad40556feffc12882839332672c0ce1e4fe80c1e2ad0327",
    "zh:e1230eb0562a5c637781af8d8f9cf356f3834762e982171c955e6ece1c002129",
    "zh:e1750fefcf2a24339697045b48acb5e0397f3726f5003a284e3e7402463bb405",
    "zh:e29d432fedbf5abb00d7653815e08a35227242bdf72b6bc0e526597431771053",
    "zh:f0ca956a985d4dfe4e39fb5bb03130c9a55ea95d2e51c5788f7352b1e61000f0",
  ]
}
